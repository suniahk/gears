import com.haxepunk.HXP;

class HelperFunctions {
	public static function fullScreenFix()
	{
		HXP.screen.scaleX = HXP.screen.scaleY = 1; // set screen scale to 1x1
		HXP.resize(HXP.stage.stageWidth, HXP.stage.stageHeight);
	}
}