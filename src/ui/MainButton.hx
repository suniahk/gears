package ui;

import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Touch;
import com.haxepunk.Tween;
import com.haxepunk.tweens.misc.AngleTween;
import com.haxepunk.HXP;

class MainButton extends Entity
{
	private var gfxList : Graphiclist;

	private var image : Spritemap;
	private var buttonText : Text;
	private var leftGear : Spritemap;
	private var rightGear : Spritemap;
	private var animateLeft : AngleTween;
	private var animateRight : AngleTween;
	private var gearCheck : Bool;
	private var buttonActive : Bool;
	private var moving : Bool;
	private var theScene : Scene;
	private var imgWidth : Int;
	private var imgHeight : Int;
	private var buttonType : String;
	private var activeFlag : Bool;


	public function new (text : String, dx : Float, dy : Float, scene : Scene, ?size : String = "large",?disabled : Bool)
	{
		super(dx, dy);

		theScene = scene;

		moving = false;
		buttonActive = true;

		if (disabled != null)
		{
			if (disabled)
			{
				buttonActive = false;
			}
		}

		gfxList = new Graphiclist();
		gearCheck = false;
		activeFlag = false;

		gfxList.add(leftGear = new Spritemap("gfx/gears/gear.png", 200, 200));
		gfxList.add(rightGear = new Spritemap("gfx/gears/gear.png", 200, 200));
		buttonType = size;

		if (size == "small")
		{
			gfxList.add(image = new Spritemap("gfx/menu/button-small.png", 517, 185));
			image.scale = (HXP.screen.width/1893)*.85;

			imgWidth = Math.round(517 * image.scale);
			imgHeight = Math.round(185* image.scale);
		} else if (size == "medium") {
			gfxList.add(image = new Spritemap("gfx/menu/button-medium.png", 740, 185));
			image.scale = (HXP.screen.width/1593)*.85;

			imgWidth = Math.round(740 * image.scale);
			imgHeight = Math.round(185* image.scale);
		} else {
			gfxList.add(image = new Spritemap("gfx/menu/button.png", 1293, 185));
			image.scale = (HXP.screen.width/1293)*.85;

			imgWidth = Math.round(1293 * image.scale);
			imgHeight = Math.round(185* image.scale);
		}

		setHitbox(imgWidth, imgHeight, Math.round(imgWidth/2), Math.round(imgHeight/2));

		gfxList.add(buttonText = new Text (text, 0, 0, 0, 0, {align:"CENTER", color: 0x000000, size:Math.round((image.height*image.scale)/2)}));

		buttonText.font = "font/DAYPBL__.ttf";

		if (!buttonActive)
		{
			image.alpha = .5;
			buttonText.alpha = .5;
		}

		leftGear.visible = false;
		rightGear.visible = false;

		leftGear.scale = rightGear.scale =image.scale * .8;

		leftGear.centerOrigin();
		rightGear.centerOrigin();

		if (size == "small")
		{
			leftGear.x = (517/2) * image.scale * .95;
			rightGear.x = leftGear.x * -1;
		} else if (size == "medium") {
			leftGear.x = (740/2) * image.scale * .95;
			rightGear.x = leftGear.x * -1;
		} else {
			leftGear.x = (1293/2) * image.scale * .95;
			rightGear.x = leftGear.x * -1;
		}

		image.centerOrigin();
		buttonText.centerOrigin();

		//leftGear.layer = 3;
		//rightGear.layer = 3;
		//image.layer = 2;
		//buttonText.layer = 1;
		leftGear.smooth = true;

		graphic = gfxList;

		animateLeft = new AngleTween(TweenType.OneShot);
		animateRight = new AngleTween(TweenType.OneShot);
	}

	public function flag ()
	{
		return activeFlag;
	}

	public function changeFlag (which : Bool)
	{
		activeFlag = which;
	}

	public function isMenuBase ()
	{
		gfxList.scrollX = 0;
		gfxList.scrollY = 0;
	}

	public function mouseOver ()
	{
		if (theScene.mouseX >= left && theScene.mouseY >= top && theScene.mouseX <= right && theScene.mouseY <= bottom)
		{
			return true;
		}
		return false;
	}

	public function mouseOut ()
	{
		if (theScene.mouseX < left || theScene.mouseY < top || theScene.mouseX > right || theScene.mouseY > bottom)
		{
			return true;
		}
		return false;
	}

	public function clicked ()
	{
		if (Input.mousePressed && this.scene != null)
        {
            if (collidePoint (this.centerX, this.centerY, theScene.mouseX, theScene.mouseY))
            { 
                return true;
            }
        }
		return false;
	}

	public function changePosition (dx : Float, dy : Float)
	{
		x = dx;
		y = dy;
	}

	public function movement (isMoving : Bool)
	{
		moving = isMoving;
	}

	public function checkMouse()
	{
		if ((mouseOver() && buttonActive && !moving) || activeFlag)
		{
			if (!gearCheck)
			{
				leftGear.visible = true;
				rightGear.visible = true;
				gearCheck = true;
			}
			
			if (!hasTween)
			{
				animateLeft.tween(leftGear.angle, leftGear.angle-90, 1);
				animateRight.tween(rightGear.angle, rightGear.angle+90, 1);

				addTween(animateLeft);
				addTween(animateRight);
			}
			return true;
		} else if ((mouseOut() || moving) && !activeFlag) {
			leftGear.visible = false;
			rightGear.visible = false;
			gearCheck = false;
		}

		return false;
	}

	public override function update ()
	{
		#if !mobile
		if (checkMouse())
		{
			leftGear.angle = animateLeft.angle;
			rightGear.angle = animateRight.angle;
		}

		
		if (image.scale != (HXP.screen.width/1293)*.85)
		{
			image.scale = (HXP.screen.width/1293)*.85;

			if (buttonType == "small")
			{
				imgWidth = Math.round(517 * image.scale);
				imgHeight = Math.round(185* image.scale);
			} else if (buttonType == "medium") {
				imgWidth = Math.round(740 * image.scale);
				imgHeight = Math.round(185* image.scale);
			} else {
				imgWidth = Math.round(1293 * image.scale);
				imgHeight = Math.round(185* image.scale);
			}

			leftGear.scale = rightGear.scale =image.scale * .8;

			if (buttonType == "small")
			{
				leftGear.x = (517/2) * image.scale * .80;
				rightGear.x = leftGear.x * -1;
			} else if (buttonType == "medium") {
				leftGear.x = (740/2) * image.scale * .92;
				rightGear.x = leftGear.x * -1;
			} else {
				leftGear.x = (1293/2) * image.scale * .95;
				rightGear.x = leftGear.x * -1;
			}
	
			buttonText.size=Math.round((image.height*image.scale)/2);
			buttonText.centerOrigin();
			setHitbox(imgWidth, imgHeight, Math.round(imgWidth/2), Math.round(imgHeight/2));
		}
		#end

		super.update();
	}
}