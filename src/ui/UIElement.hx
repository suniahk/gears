package ui;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Input;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Image;

class UIElement extends Entity
{
	private var text : Text;
	private var img : Image;

	private var gfxList : Graphiclist;


	public function new (theText : String, dx : Int, dy : Int, ?setSize : Int = 18)
	{
		super(dx, dy);

		gfxList = new Graphiclist();
		gfxList.add(text = new Text (theText, 0, 0, 0, 0, {align:"CENTER", color: 0x000000, size: setSize}));
		text.font = "font/ANUDI___.ttf";

		//text.setOrigin(0, text.width/2);

		text.originX = Math.round(text.width);
		text.originY = Math.round(text.height/2);

		graphic = gfxList;

		layer = 1;
		//type = "text";
	}

	public function updateScore (newText : String)
	{
		text.text = newText;
		text.originX = Math.round(text.width);
		text.originY = Math.round(text.height/2);
		//text.centerOrigin();
	}

	public function setColor (color : Int)
	{
		text.color = color;
	}

	public function setSize(newSize : Int)
	{
		text.size = newSize;
		text.originX = Math.round(text.width);
		text.originY = Math.round(text.height/2);
		//text.centerOrigin();
	}

	public function altFont ()
	{
		text.font = "font/DAYPBL__.ttf";
		text.originX = Math.round(text.width);
		text.originY = Math.round(text.height/2);
		//text.centerOrigin();
	}

	public function getSize() : Int
	{
		return text.size;
	}

	public function updateLocation (dx : Int, dy : Int)
	{
		x = dx;
		y = dy;
	}

	public override function update ()
	{
		super.update();
	}
}