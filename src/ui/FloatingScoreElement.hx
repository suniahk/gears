package ui;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Input;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Image;
import com.haxepunk.Tween;
import com.haxepunk.tweens.motion.LinearMotion;
import com.haxepunk.utils.Ease;

class FloatingScoreElement extends Entity
{
	private var text : Text;
	private var outline : Text;
	private var movement : LinearMotion;
	private var finished : Bool;

	private var gfxList : Graphiclist;

	public function new (theText : String, dx : Int, dy : Int)
	{
		super(dx, dy);

		gfxList = new Graphiclist();
		finished = false;

		movement = new LinearMotion(isFinished, TweenType.OneShot);

		movement.x = x;
		movement.y = y;

		gfxList.add(outline = new Text (theText, 0, 0, 0, 0, {align:"CENTER", color: 0x000000}));
		gfxList.add(text = new Text (theText, 0, 0, 0, 0, {align:"CENTER", color: 0xffffff}));
		text.font = "font/belligerent.ttf";
		outline.font = "font/belligerent.ttf";

		outline.x += 1;
		outline.y += 2;

		text.centerOrigin();
		outline.centerOrigin();
		text.size = 20;
		outline.size = 20;

		graphic = gfxList;
		layer = 0;
	}

	public function checkFinished ()
	{
		return finished;
	}

	public function isFinished(event:Dynamic)
	{
		finished = true;
	}

	public function updateScore (newText : String)
	{
		text.text = newText;
		text.centerOrigin();
	}

	public function setSize(newSize : Int)
	{
		text.size = newSize;
	}

	public function getSize() : Int
	{
		return text.size;
	}

	public function animateScore(scoreX : Int, scoreY : Int)
	{
		movement.setMotion(x, y, scoreX, scoreY, 1.5, Ease.expoIn);

		this.addTween(movement);
	}

	public override function update ()
	{
		x = movement.x;
		y = movement.y;
		super.update();
	}

}