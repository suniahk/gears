package ui;

import ui.FloatingScoreElement;
import com.haxepunk.Scene;
import GameLogic;

class FloatingTracker
{
	private var points : Array<FloatingScoreElement>;

	public function new ()
	{
		points = [];
	}

	public function scoreVisual (scene : Scene, gears : entities.GearGrid, scoreUI : ui.UIElement, line : Int, ?isCol : Bool)
	{
		var lineX : Int;
		var lineY : Int;
		var found : Bool = false;
		var loc : Int = 0;

		if (isCol == null)
		{
			isCol = false;
		}

		if (isCol)
		{
			lineY = gears.avgY(line, true);
			lineX = gears.avgX(line);
		} else {
			lineX = gears.avgX(line, true);
			lineY = gears.avgY(line);
		}

		for ( count in 0 ... points.length )
		{
			if (points[count] == null)
			{
				if (!found)
				{
					points[count] = new FloatingScoreElement(Std.string(GameLogic.viewValue()), lineX, lineY);
					loc = count;
				}
				found = true;
			}
		}

		if (!found)
		{
			loc = points.length;
			points[points.length] = new FloatingScoreElement(Std.string(GameLogic.viewValue()), lineX, lineY);
		}
		scene.add (points[loc]);

		points[loc].animateScore (Std.int(scoreUI.x), Std.int(scoreUI.y));
	}

	public function removeScoreVisual (scene : Scene)
	{
		for ( count in 0 ... points.length )
		{
			if (points[count]!= null)
			{
				if (points[count].checkFinished())
				{
					scene.remove(points[count]);
					if (count == points.length )
					{
						points.splice(count, 1);
					}
					points[count] = null;
				}
			}
		}
	}
}