package entities;
 
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Touch;
import com.haxepunk.Tween;
import com.haxepunk.tweens.misc.AngleTween;
import com.haxepunk.tweens.motion.LinearMotion;
import com.haxepunk.utils.Ease;
import com.haxepunk.graphics.Graphiclist;
import entities.GearType;
import com.haxepunk.HXP;
 
class Gear extends Entity
{
    private var gfxList : Graphiclist;

	private var image : Spritemap;
    private var imgBar : Spritemap;
    private var statusImg : Spritemap;
    private var rotateDirection : Int;
    private var needRotate : Bool;
    private var animate : AngleTween;
    private var animateBar : AngleTween;
    private var orient : String;
    public var shift : LinearMotion;
    private var check : Bool;
    private var dyn : Dynamic;
    private var blocked : Bool;
    private var second : String;
    private var setAngle : Float;
    private var needsReverse : Bool;
    private var gearInfo : GearType;
    private var gearFrame : Int;
    private var gearSize : Int;
    private var interval : Int;
    private var gearImages : Array <Image>;
    private var scaledGearSize : Int;
    private var gearReset : Bool;
    private var scaleFactor : Int;

    public function orientation()
    {
        return orient;
    }

    public function newGearType ()
    {
        gearInfo = new GearType();
    }

    private function onGearClick ()
    {
        if (Input.mousePressed)
        {
            if (collidePoint (this.centerX, this.centerY, Input.mouseX, Input.mouseY))
            {
                needRotate = true;
            }
        }
    }

    /*private function approachScale (distanceToCenter : Float, ?offset : Float = 23) : Float
    {
        var distanceX : Float = Math.max(Input.mouseX - x, x - Input.mouseX);
        var distanceY : Float = Math.max(Input.mouseY - y, y - Input.mouseY);
        if (distanceX >= distanceY && distanceX <= distanceToCenter)
        {
            return .9 + Math.min(((distanceToCenter - distanceX)/(distanceToCenter - offset)) * .1, .1);
        } else if (distanceX < distanceY && distanceY <= distanceToCenter) {
            return .9 + Math.min(((distanceToCenter - distanceY)/(distanceToCenter - offset)) * .1, .1);
        }

        return .9;
    }*/

    public function rotateGear (amount:Int, duration: Float) 
    {
        animate = new AngleTween(setCheck,TweenType.OneShot);
        if (rotateDirection == 1)
        {
            animate.tween(image.angle, image.angle+amount, duration);

        } else {
            animate.tween(image.angle, image.angle-amount, duration);
        }

        addTween (animate);
        needRotate = false;
    }

    public function shiftDown (?numPositions:Int = 1) {
        shift.setMotion(x, y, x, y+(interval*numPositions), 0.75, Ease.bounceOut);

        addTween(shift);
    }

    public function shiftLeft (?numPositions:Int = 1) {
        shift.setMotion(x, y, x+(interval*numPositions), y, 0.75, Ease.bounceOut);

        addTween(shift);
    }

    public function clicked () { return needRotate; }

    public function rDirection () { return rotateDirection; }

    public function setCheck (event:Dynamic)
    {
        if (orient=="-")
        {
            orient="|";
        } else if (orient == "|") {
            orient="-";
        }

        check = true;
    }

    public function resetCheck () { check = false; }

    public function canCheck () : Bool { return check; }

    public function isBlocked () { return blocked; }

    public function gearSetup (?stepX : Int = 200, ?stepY : Int = 200)
    {
        var whichGear : String;
        switch (rotateDirection)
        {
            case 1 :
                whichGear = "_offset";
            default :
                whichGear = "";
        }
        switch (orient)
        {
            case "-" :
                return new Spritemap("gfx/gears/gear_horizontal" + whichGear + ".png", stepX, stepY);
            case "|" :
                return new Spritemap("gfx/gears/gear_vertical" + whichGear + ".png", stepX, stepY);
            case "+" :
                return new Spritemap("gfx/gears/gear_plus" + whichGear + ".png", stepX, stepY);
            default :
                return null;
        }
    }

    public function new(dx:Int, dy:Int, shouldRotate : Int)
    {
        gearSize = 200;
        gearReset = false;
        scaleFactor = 2300;

        super(dx, dy);

        blocked = false;
        needRotate = false;

        gfxList = new Graphiclist();

 		animate = new AngleTween(setCheck,TweenType.OneShot);

        shift = new LinearMotion(TweenType.OneShot);
        shift.x = dx;
        shift.y = dy;

        rotateDirection = shouldRotate;

        newGearType();
        orient = gearInfo.returnType();
        second = gearInfo.returnStatus();

 		gfxList.add(image = gearSetup());

        image.scale = (HXP.screen.width/8)/gearSize;
        scaledGearSize = Math.round(gearSize * image.scale);
        interval = Math.floor(scaledGearSize * 0.94);

        checkStatusChanges();
        image.centerOrigin();

        image.smooth = true;

        graphic = gfxList;

        setHitbox (scaledGearSize, scaledGearSize, Math.round(scaledGearSize/2), Math.round(scaledGearSize/2));
        layer = 10;
        setAngle = -1;
    }

    public function checkStatusChanges()
    {
        if (second == "red") {
            blocked = true;
            gfxList.remove(statusImg);
            gfxList.add(statusImg = new Spritemap ("gfx/Lock.png", gearSize, gearSize));
            statusImg.centerOrigin();
            statusImg.scale = (HXP.screen.width/8)/gearSize;
        } else if (second == "normal") {
            blocked = false;
            gfxList.remove(statusImg);
        }

        graphic = gfxList;
    }

    public function getSecond()
    {
        return second;
    }

    public function getGearType(which : Int)
    {
        if (which == 0)
        {
            return gearInfo.typeNum(which, orient);
        } else if (which == 1) {
            return gearInfo.typeNum(which, second);
        }

        return -1;
    }

    public function newStatus ()
    {
        gearInfo.newSecondary();
        second = gearInfo.returnStatus();
        checkStatusChanges();
    }

    public function badRotate ()
    {
        needsReverse = true;
        setAngle = image.angle;
        needRotate = false;

        animate = new AngleTween(TweenType.OneShot);

        animate.tween(image.angle, image.angle+5, .05);

        addTween(animate);
    }

    private function badRotateReverse ()
    {
        needsReverse = false;
        animate = new AngleTween(TweenType.OneShot);

        animate.tween(image.angle, image.angle-5, .05);

        addTween(animate);
    }

    private function gearStep (x : Int, y : Int)
    {
        if (second == "red")
        {
            gfxList.remove(statusImg);
            gfxList.add(statusImg = new Spritemap ("gfx/Lock.png", Math.round(Math.min(x, 200)), Math.round(Math.min(y, 200))));
            statusImg.centerOrigin();
            statusImg.angle += 180;
            statusImg.scale = (HXP.screen.width/8)/gearSize;
        }

        gfxList.remove(image);

        gfxList.add(image = gearSetup(Math.round(Math.min(x, 200)), Math.round(Math.min(y, 200))));
        image.centerOrigin();

        image.angle += 180;
    }

    private function hideGear () // Needs to be rewritten to be more clear and concise.  Not liking the code currently.  May change how this works anyway.
    {
        var leftThreshold : Int = Math.round(HXP.screen.width * .1)+Math.round(scaledGearSize/2);
        var topThreshold : Int = Math.round(HXP.screen.height * .03)+Math.round(scaledGearSize/2);
        var oldScale : Float = image.scale;

        if (x < leftThreshold)
        {
            var step = Math.floor((scaledGearSize - (leftThreshold - x))/oldScale);

            if (step <= 0)
            {
                gfxList.remove(image);
                gearReset = true;
                if (second == "red")
                {
                    gfxList.remove(statusImg);
                }
            } else {
                gearStep (step, gearSize);
                
                image.x = ((gearSize - step) * oldScale)/2;
            }

            graphic = gfxList;
        } else if (y < topThreshold)
        {
            var step = Math.floor((scaledGearSize - (topThreshold - y))/oldScale);

            if (step <= 0)
            {
                gfxList.remove(image);
                gearReset = true;
                if (second == "red")
                {
                    gfxList.remove(statusImg);
                }
            } else {
                gearStep (gearSize, step);

                image.y = ((gearSize - step) * oldScale)/2;
            }
            graphic = gfxList;
        } else if (image.width != gearSize || image.height != gearSize) {
            gearStep (gearSize, gearSize);
            
            graphic = gfxList;
        } else if (gearReset) {
            if (second == "red")
            {
                gfxList.add(statusImg = new Spritemap ("gfx/Lock.png", gearSize, gearSize));
                statusImg.centerOrigin();
                statusImg.angle += 180;
                statusImg.scale = (HXP.screen.width/8)/gearSize;
                
            }

            gfxList.add(image = gearSetup());
            image.centerOrigin();
            
            graphic = gfxList;

            gearReset = false;
        }

        if (image.scale != (HXP.screen.width/8)/gearSize) {image.scale = (HXP.screen.width/8)/gearSize;}
    }

    public override function update ()
    {
        onGearClick();   

        hideGear();

        if (needsReverse && image.angle == (setAngle + 5))
        {
            badRotateReverse();
        }

        if (x != shift.x) {x = shift.x;}
        if (y != shift.y) {y = shift.y;}
        if (image.angle != animate.angle) {image.angle = animate.angle;}

    	super.update();
    }
}