package entities;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Emitter;
import com.haxepunk.utils.Ease;

class ClearEmitter extends Entity
{
	private var _emit : Emitter;

	public function new ()
	{
		super (x, y);

		_emit = new Emitter("gfx/gearParticle.png", 2, 2);
        _emit.newType("gearClear", [0]);
        _emit.setAlpha("gearClear", 1, 0.1);
        _emit.setMotion("gearClear", 0, 10, 0.2, 360, -4, 1, Ease.quadOut);

        graphic = _emit;
        layer = 1;
        type = "particle";
	}

	public function enable (dx : Float, dy : Float)
	{
		for(i in 0...80) {
        	_emit.emitInCircle("gearClear", dx, dy, 23);
        }
	}

	public function sparkle ()
	{
		for(i in 0...100) {
        	_emit.emit("gearClear", 223, 23);
        }
	}

	public override function update ()
	{
		super.update();
	}
}