package entities;

import com.haxepunk.Entity;
import com.haxepunk.Scene;
import GameLogic;
import entities.Gear;
import entities.ClearEmitter;
import com.haxepunk.HXP;

class GearGrid // GearGrid is an array of entities, but isn't one itself.
{
	private var gears : Array<Array<Gear>>;
	private var rotate : Int;
	private var baseX : Int;
	private var baseY : Int;
	private var maxCount : Array<Array<Int>>;
	private var currentCount : Array<Array<Int>>;
	private var particleEffect : ClearEmitter;
	private var amountBetween: Int;
	private var scaleFactor : Int;

	public function new (xLen : Int, yLen : Int, ?dx : Int = 100, ?dy : Int = 50)
	{
		scaleFactor = 2300;
		amountBetween = Math.round((HXP.screen.width/8)*.94); //Math.floor((200 * (HXP.screen.height/scaleFactor))* 0.94);

		setCounts(1);

		baseX = dx;
		baseY = dy;
		rotate = 0;
		particleEffect = new ClearEmitter();

		gears = [];
		for ( x in 0 ... xLen )
		{
			gears[x] = [];
			for ( y in 0 ... yLen )
			{
		    	createNewGear(x, y, (x * amountBetween) + dx, (y * amountBetween) + dy);

				rotate = (rotate - 1) * (-1); 
			}
			rotate = (rotate - 1) * (-1);
		}
	}

	public function updateScaledPosition ()
	{
		amountBetween = Math.round((HXP.screen.width/8)*.94); //Math.floor((200 * (HXP.screen.height/scaleFactor))* 0.94);
		for ( x in 0 ...  gears.length )
		{
			for ( y in 0 ... gears[x].length )
			{
				gears[x][y].shift.x = (x * amountBetween) + baseX;
				gears[x][y].shift.y = (y * amountBetween) + baseY;
			}
		}
	}

	public function updateBase (dx : Int, dy : Int)
	{
		baseX = dx;
		baseY = dy;
	}

	private function createNewGear (x : Int, y : Int, gearX : Int, gearY : Int)
	{
		var count = 0;
		var redo = false;

		do {
			if (redo)
			{
				currentCount[0][count] = Std.int(Math.max(0, currentCount[0][count] - 1));
			}

			gears[x][y] = new Gear(gearX, gearY, rotate);
			count = gears[x][y].getGearType(0);
			currentCount[0][count]++;

			if (maxCount[0][count] == -1)
			{
				break;
			}

			redo = true;
		} while (maxCount[0][count] < currentCount[0][count]);

		count = gears[x][y].getGearType(1);
		currentCount[1][count]++;

		if (maxCount[1][count] != -1)
		{
			while (maxCount[1][count] < currentCount[1][count]) {
				currentCount[1][count] = Std.int(Math.max(0, currentCount[1][count] - 1));
	
				gears[x][y].newStatus();
				count = gears[x][y].getGearType(1);
				currentCount[1][count]++;

				if (maxCount[1][count] == -1)
				{
					break;
				}
			}
		}
	}

	private function setCounts (level : Int)
	{
		maxCount = [];

		maxCount[0] = [];
		maxCount[0][0] = -1;
		maxCount[0][1] = -1;
		maxCount[0][2] = -1;

		maxCount[1] = [];
		maxCount[1][0] = -1;
		maxCount[1][1] = -1;
		maxCount[1][2] = 0;
		maxCount[1][3] = 1;

		currentCount = [];

		currentCount[0] = [];
		currentCount[0][0] = 0;
		currentCount[0][1] = 0;
		currentCount[0][2] = 0;

		currentCount[1] = [];
		currentCount[1][0] = 0;
		currentCount[1][1] = 0;
		currentCount[1][2] = 0;
		currentCount[1][3] = 0;
	}

	public function toggle (scene : Scene) // Needs to be scene.add and scene.remove
	{
		for (line in 0 ... 6)
		{
			if (gears[line][0].scene != null)
			{
				scene.removeList(gears[line]);
			} else {
				scene.addList(gears[line]);
			}
		}

		return Std.string(gears[0][0].scene);
	}

	public function checkValid()
	{
		var checker = true;
		
		for (x in 0...6)
		{
			for (y in 0 ... 8)
			{
				if (gears[x][y].canCheck())
				{
					gears[x][y].resetCheck();
					checker = false;
					GameLogic.setRotate();
				} else if (GameLogic.checkLineFound() && gears[x][y].hasTween) {
					GameLogic.setRotate();
					checker = false;
				}
			}
		}

		return checker;
	}

	public function checkRow ()
	{
		return checkHorizontal();
	}

	public function checkCol ()
	{
		return checkVertical();
	}

	public function length()
	{
		return gears.length;
	}

	public function getCol(line : Int)
	{
		return gears[line];
	}

	public function getRow(line : Int)
	{
		var result : Array<Gear>;
		result = [];
		for (i in 0 ... gears.length)
		{
			result[i] = gears[i][line];
		}

		return result;
	}

	public function removeRow (scene : Scene, line : Int, reset : Bool)
	{
		rotate = gears[0][line].rDirection();
		scene.add(particleEffect);
		var position = baseX - amountBetween;

		for (gear in getRow(line))
		{
			if (gear.getSecond()=="red")
			{
				currentCount[1][3]--;
			}
			if (!reset){particleEffect.enable(gear.x, gear.y);}
		}
		scene.removeList(getRow(line));
		for ( x in 0 ... 6 )
		{
			if (reset)
			{
				position = baseX + (amountBetween * x);
			}

			createNewGear(x, line, position, (line * amountBetween) + baseY);

			scene.add (gears[x][line]);
			rotate = (rotate - 1) * (-1); 

			if (!reset){gears [x][line].shiftLeft(x+1);}
		}
	}

	public function removeColumn (scene : Scene, line : Int, reset : Bool)
	{
		rotate = gears[line][0].rDirection();
		scene.add(particleEffect);
		var position = baseY - amountBetween;

		for (gear in gears[line])
		{
			if (gear.getSecond()=="red")
			{
				currentCount[1][3]--;
			}
			if (!reset){particleEffect.enable(gear.x, gear.y);}
		}

		scene.removeList(gears[line]);
		for ( y in 0 ... 8 )
		{
			if (reset)
			{
				position = baseY + (amountBetween * y);
			}
			createNewGear(line, y, (line * amountBetween) + baseX, position);

			scene.add (gears[line][y]);
			rotate = (rotate - 1) * (-1);

			if (!reset){gears [line][y].shiftDown(y+1);}
		}
	}

	public function avgX (line : Int, ?row : Bool)
	{
		if (row != null && row)
		{
			return Math.ceil((gears[4][line].x + gears[5][line].x) / 2);
		} else {
			return Math.ceil(gears[line][4].x + 20);
		} 
	}

	public function avgY (line : Int, ?row : Bool)
	{
		if (row != null && row)
		{
			return Math.ceil((gears[line][4].y + gears[line][5].y) / 2);
		} else {
			return Math.ceil(gears[4][line].y + 20);
		}
	}

	// Checks above, below, and side to side to see if a gear is in motion.  
	public function gearsBlocked(x:Int, y:Int) {
		var x2 = Math.round(Math.min(x+1, 5));
		var x3 = Math.round(Math.max(x-1, 0));
		var y2 = Math.round(Math.min(y+1, 7));
		var y3 = Math.round(Math.max(y-1, 0));

		if (gears[x][y].hasTween || gears[x2][y].hasTween || gears[x3][y].hasTween || gears[x][y2].hasTween || gears[x][y3].hasTween ||
			gears[x][y].isBlocked() || gears[x2][y].isBlocked() || gears[x3][y].isBlocked() || gears[x][y2].isBlocked() || gears[x][y3].isBlocked())
		{
			return true;
		}

		return false;
	}

	public function onGearClick(?rotateAmount:Int, ?rotateDuration:Float)
	{
		if (rotateDuration == null)
		{
			rotateDuration = 0.4;
		}

		if (rotateAmount == null)
		{
			rotateAmount = 90;
		}

		for ( x in 0 ... 6 )
		{
			for ( y in 0 ... 8 )
			{
				if (gears[x][y].clicked() && !gearsBlocked(x, y))
				{
					gears[x][y].rotateGear (rotateAmount, rotateDuration);
					if (x+1 <= 5)
					{
						gears[x+1][y].rotateGear (rotateAmount, rotateDuration);
					}
					if (x-1 >= 0)
					{
						gears[x-1][y].rotateGear (rotateAmount, rotateDuration);
					}
					if (y+1 <= 7)
					{
						gears[x][y+1].rotateGear (rotateAmount, rotateDuration);
					}
					if (y-1 >= 0)
					{
						gears[x][y-1].rotateGear (rotateAmount, rotateDuration);
					}
				} else if (gears[x][y].clicked() && gearsBlocked(x, y) && !gears[x][y].hasTween) {
					gears[x][y].badRotate();
				}
			}
		}
	}

	public function checkVertical () {
		var check = true;
		for (x in 0 ... 6) {
			check = true;
			for (y in 0 ... 8) {
				if (gears[x][y].orientation() != "|" && gears[x][y].orientation() != "+")
				{
					check = false;
					break;
				}
				if (gears[x][y].hasTween)
				{
					check = false;
					break;
				}
			}
			if (check)
			{
				return x;
			}
		}

		return -1;
	}

	public function checkHorizontal () {
		var check = true;
		for (y in 0 ... 8) {
			check = true;
			for (x in 0 ... 6) {
				if (gears[x][y].orientation() != "-" && gears[x][y].orientation() != "+")
				{
					check = false;
					break;
				}
				if (gears[x][y].hasTween)
				{
					check = false;
					break;
				}
			}
			if (check)
			{
				return y;
			}
		}
		return -1;
	}
}