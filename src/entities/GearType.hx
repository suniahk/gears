package entities;

class GearType
{
	private var type : Array<String>;
	private var orient : String;
	private var status : Array<String>;
	private var secondary : String;
	private var diceRoll : Int;
	private var chances : Array<Array<Int>>;
	private var tracker : Int;
	private var count : Int;

	private function setTypes ()
	{
		type = [];
		// Basic Gears
		type [0] = "-";
		type [1] = "|";
		type [2] = "+";

		status = [];
		// Status Effects
		status[0] = "normal";
		status[1] = "scoreBonus";
		status[2] = "orange";
		status[3] = "red";
	}

	private function setChances (mode : Int) // Chances don't change per level. they chance per mode
	{
		var greenChance : Array<Int> = [0, 0, 0];
		var orangeChance : Array<Int> = [0, 0, 0];
		var redChance : Array<Int> = [5, 5, 5];

		// NOTE: Chances are a percentage out of 100.
		chances = [];
		chances[0] = [];

		chances[0][2] = 10; // Plus Gears

		chances[0][0] = Math.floor((100 - chances[0][2]) /2);
		chances[0][1] = (Math.floor((100 - chances[0][2]) /2)) + (100 - chances[0][2]) % 2;

		chances[1] = [];

		chances[1][1] = greenChance[mode];
		chances[1][2] = orangeChance[mode];
		chances[1][3] = redChance[mode];

		chances[1][0] = 100 - (chances[1][1] + chances[1][2] + chances[1][3]);
	}

	public function new (?level : Int)
	{
		if (level == null)
		{
			level = 0;
		}

		setTypes();
		setChances(level);

		diceRoll = Std.random(100);

		tracker = 0;
    	count = 0;
        for (element in chances[0])
        {
        	if (diceRoll >= tracker && diceRoll <= (element + tracker) - 1)
        	{
        		orient = type[count];
        		break;
        	}
        	tracker += element;
        	count++;
        }


        diceRoll = Std.random(100);

        tracker = 0;
        count = 0;
        for (element in chances[1])
        {
        	if (diceRoll >= tracker && diceRoll <= (element + tracker) - 1)
        	{
        		secondary = status[count];
        		break;
        	}
        	tracker += element;
        	count++;
        }
        
	}

	public function typeNum (which : Int, object : String)
	{
		if (which == 0)
		{
			for (i in 0 ... type.length)
			{
				if (type[i]==object)
				{
					return i;
				}
			}
		} else if (which == 1) {
			for (i in 0 ... status.length)
			{
				if (status[i]==object)
				{
					return i;
				}
			}
		}

		return -1;
	}

	public function newSecondary ()
	{
		diceRoll = Std.random(100);

        tracker = 0;
        count = 0;
        for (element in chances[1])
        {
        	if (diceRoll >= tracker && diceRoll <= (element + tracker) - 1)
        	{
        		secondary = status[count];
        		break;
        	}
        	tracker += element;
        	count++;
        }
	}

	public function returnType ()
	{
		return orient;
	}

	public function returnStatus ()
	{
		return secondary;
	}
}