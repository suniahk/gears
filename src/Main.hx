import com.haxepunk.Engine;
import com.haxepunk.HXP;

class Main extends Engine
{

	override public function init()
	{
#if debug
		HXP.console.enable();
#end
	
		HXP.screen.scale = 1;//Math.min(HXP.screen.width/2000, HXP.screen.height/1500);
		HXP.scene = new scenes.IntroScene();
	}

	public static function main() { 
		new Main();
	}
}