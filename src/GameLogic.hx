import com.haxepunk.Entity;
import com.haxepunk.Scene;
import entities.Gear;

class GameLogic extends Scene
{
	private static var lineFound : Bool;
	private static var didRotate : Bool;
	private static var score : Int;
	private static var lineValue : Int;
	private static var multiplier : Float;
	private static var increaseMultiplierBy : Float;
	private static var maxMult : Int;
	private static var punish : Float;
	private static var timeBonus : Int;
	private static var timeLapse : Int;
	private static var bonusTime : Bool;
	private static var trigger : Bool;
	private static var timeCheck : Int;

	public static function increaseScore() { score += lineValue; }

	public static function getScore() { return score; }

	public static function getMult() {
		if (bonusTime)
		{
			return multiplier * 2;
		} else {
			return multiplier;
		}
	}

	public static function checkLineFound() { return lineFound; }

	public static function foundLine() { lineFound = true; }

	public static function resetLine() { lineFound = false; }

	public static function setValue(amount:Int) { lineValue = amount; }

	public static function viewValue() { return lineValue*multiplier; }

	public static function setMult(amount:Float) { multiplier = amount; }

	public static function applyPunishment() { multiplier = Math.max(multiplier - punish, 1); }

	public static function setPunishment(amount : Float) { punish = amount; }

	public static function setMultStep(amount:Float) { increaseMultiplierBy = amount; }

	public static function setMaxMult(amount:Int) { maxMult = amount; }

	public static function getMaxMult () { return maxMult; }

	public static function resetScore() { score = 0; }

	public static function resetMult() { multiplier = 1; }

	public static function setRotate() { didRotate = true; }

	public static function resetRotate() { didRotate = false; }

	public static function checkRotate () { return didRotate; }

	public static function setTimeoutBonus (timer : Int) { timeBonus = timer; }

	public static function getBonusTime () { return bonusTime; }

	public static function bonusTrigger () { 
		if (trigger) {
			trigger = false;
			return true;
		}
		return trigger;
	}

	public function new()
	{
		super();
	}

	public override function begin()
	{
		bonusTime = false;
		timeLapse = 0;
		trigger = false;
	}

	public static function modifyScore ()
	{
		if (bonusTime == false)
		{
			score += Math.round(lineValue * multiplier);
			bonusTime = true;
			trigger = true;
		} else {
			score += Math.round(lineValue * multiplier * 2);
		}
		timeLapse = Date.now().getSeconds();
		multiplier = Math.min(multiplier + increaseMultiplierBy, maxMult);
	}

	public static function bonusWatch ()
	{
		timeCheck = (Date.now().getSeconds() == 0)? 60 : Date.now().getSeconds();
		//trace (bonusTime);
		if ((timeLapse + timeBonus) < timeCheck && bonusTime)
		{
			bonusTime = false;
			trigger = true;
		}
	}

	public static function checkPunish ()
	{
		if (GameLogic.checkLineFound())
		{
			GameLogic.resetLine();
		} else {
			GameLogic.applyPunishment();
		}
	}
}