package scenes;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.graphics.Image;
import GameLogic;
import entities.Gear;
import entities.GearGrid;
import ui.UIElement;
import ui.FloatingScoreElement;
import ui.FloatingTracker;
import ui.MainButton;
import HelperFunctions;

class GameScene extends Scene
{
	private var gears : GearGrid;
	private var scoreUI : UIElement;
	private var scoreLabel : UIElement;
	private var multiplyUI : UIElement;
	private var multiplyLabel : UIElement;
	private var points : FloatingTracker;
	private var menuButton : MainButton;
	private var toggleStatus : String;
	private var inGameMenu : Array<MainButton>;
	private var back : Entity;
	private var graphic : Image;
	private var scaledGearWidth : Float;
	private var multSize : Int;
	private var multBack : Entity;
	private var multBackImg : Image;

	private function setupClassVars()
	{
		GameLogic.setValue(100);
		GameLogic.setMult(1);
		GameLogic.setMultStep(1);
		GameLogic.setMaxMult(10);
		GameLogic.setPunishment(5);
		GameLogic.resetRotate();
		GameLogic.setTimeoutBonus(2);
	}

	private function createUI () // Note to self: Score should be right aligned
	{
		//scoreLabel = new UIElement ("Score", Math.round(HXP.stage.stageWidth/5), Math.round((HXP.stage.stageHeight/8)*7), Math.round(HXP.screen.height*.07));
		scoreUI = new UIElement (Std.string(GameLogic.getScore()), Math.round(HXP.stage.stageWidth*.62), Math.round(HXP.stage.stageHeight*.90), Math.round(HXP.screen.height*.07));

		multSize = Math.round(HXP.screen.height*.05);
		//multiplyLabel = new UIElement ("Multiplier", Math.round((HXP.stage.stageWidth/4) * 3), Math.round((HXP.stage.stageHeight/8)*7), Math.round(HXP.screen.height*.07));
		multiplyUI = new UIElement (Std.string(GameLogic.getMult())+"x", Math.round(HXP.stage.stageWidth*.79), Math.round(HXP.stage.stageHeight*.90), Math.round(HXP.screen.height*.05));

		/*multBackImg = new Image ("gfx/mult-base.png");
		multBackImg.scale = (HXP.stage.stageWidth * .2)/212;
		multBack = new Entity (HXP.stage.stageWidth, HXP.stage.stageHeight, multBackImg);
		multBackImg.centerOrigin();
		add (multBack);*/

		multBackImg = new Image ("gfx/ui-base.png");
		multBackImg.scale = (HXP.screen.width*.66)/1475;
		multBack = new Entity (Math.round(HXP.screen.width/2), Math.round(HXP.stage.stageHeight*.90), multBackImg);
		multBackImg.centerOrigin();
		add (multBack);

		menuButton = new MainButton ("Menu", Math.round(HXP.stage.stageWidth/4)*3, Math.round(HXP.stage.stageHeight*.05), this, "small");

		inGameMenu = [];

		inGameMenu[0] = new MainButton ("Quit Game", HXP.screen.width/2, HXP.screen.height/3, this, "medium");

		//scoreLabel.altFont();
		//multiplyLabel.altFont();

		//add (scoreLabel);
		add (scoreUI);

		//add (multiplyLabel);
		add (multiplyUI);

		//add (menuButton);
	}

	private function updateUI()
	{
		#if !mobile
		if (multSize != Math.round(HXP.screen.height*.05))
		{multSize = Math.round(HXP.screen.height*.05);}
		#end

		points.removeScoreVisual(this);
		scoreUI.updateScore(Std.string(GameLogic.getScore()));
		//multiplyUI.setSize (multSize + Math.ceil(GameLogic.getMult() * (multSize / GameLogic.getMaxMult())));
		GameLogic.bonusWatch();

		if (GameLogic.bonusTrigger())
		{
			if (GameLogic.getBonusTime())
			{
				multiplyUI.setColor(0xFF0000);
			} else if (!GameLogic.getBonusTime())
			{
				multiplyUI.setColor(0x000000);
			}
		}
		multiplyUI.updateScore(Std.string(GameLogic.getMult()) + "x");

		#if !mobile
		//scoreLabel.updateLocation(Math.round(HXP.stage.stageWidth/5), Math.round((HXP.stage.stageHeight/8)*7));
		scoreUI.updateLocation(Math.round(HXP.stage.stageWidth*.62), Math.round(HXP.stage.stageHeight*.90));
		//multiplyLabel.updateLocation(Math.round((HXP.stage.stageWidth/4) * 3), Math.round((HXP.stage.stageHeight/8)*7));
		//multiplyUI.updateLocation(Math.round((HXP.stage.stageWidth/4) * 3), Math.round(HXP.stage.stageHeight/8) + Math.round(HXP.screen.height*.07));

		menuButton.x = Math.round(HXP.stage.stageWidth/4)*3;
		menuButton.y = Math.round(HXP.stage.stageHeight*.05);
		#end

		// Make this it's own function or something
		multiplyUI.x = Math.round(HXP.stage.stageWidth*.79) + ((Std.random(Math.round(GameLogic.getMult()-1)*2) - Math.round(GameLogic.getMult()-1))/3);
		multiplyUI.y = Math.round(Math.round(HXP.stage.stageHeight*.90) + ((Std.random(Math.round(GameLogic.getMult()-1))*2) - Math.round(GameLogic.getMult()-1))/3);
	}

	private function gridChecks (?forceCheck : Bool, ?resetScore : Bool) {
		var checker = gears.checkValid();

		if (forceCheck == true)
		{
			GameLogic.setRotate();
		}

		if (GameLogic.checkRotate() && checker) {
			GameLogic.resetRotate();

			var hor = gears.checkRow();
			var ver = gears.checkCol();

			if (hor == -1 && ver == -1)
			{
				GameLogic.checkPunish();
			} else {
				GameLogic.resetLine();
				while (hor!=-1)
				{
					if (!resetScore) {points.scoreVisual(this, gears, scoreUI, hor);}
					gears.removeRow (this, hor, resetScore);
					GameLogic.modifyScore();
					hor = gears.checkRow();

					GameLogic.foundLine();
				}

				if (!GameLogic.checkLineFound())
				{
					while (ver!=-1)
					{
						if (!resetScore) {points.scoreVisual(this, gears, scoreUI, ver, true);}
						gears.removeColumn (this, ver, resetScore);
						GameLogic.modifyScore();
						ver = gears.checkCol();
	
						GameLogic.foundLine();
					}
				}
			}
		}
		
		if (resetScore == true)
		{
			GameLogic.resetScore();
			GameLogic.resetMult();
		}
	}

	public function new()
	{
		super();
	}

	public override function begin()
	{
		setupClassVars();

		scaledGearWidth = Math.round( /*200 * (HXP.screen.height/2300)*/ (HXP.screen.width/8) );

		points = new FloatingTracker();

		gears = new GearGrid(6, 8, Math.round((HXP.screen.width - ((2*scaledGearWidth) + (4*(scaledGearWidth* 0.94))))/2)+Math.round(scaledGearWidth/2), Math.round(HXP.screen.height * .03) + Math.round((scaledGearWidth/6)*5));

		for (list in 0 ... gears.length())
		{
			addList(gears.getCol(list));
		}

		createUI();

		gridChecks (true, true);
	}

	public override function update ()
	{
		if (menuButton.clicked())
		{
			toggleStatus = gears.toggle(this);
			if (toggleStatus != "null")
			{
				addList(inGameMenu);
			} else {
				removeList(inGameMenu);
			}
		}

		if (inGameMenu[0].clicked())
		{
			HXP.scene = new scenes.IntroScene();
		}

		gears.onGearClick();

		#if !mobile
		gears.updateScaledPosition();
		if (scaledGearWidth != Math.round(HXP.screen.width/8))
		{
			scaledGearWidth = Math.round(HXP.screen.width/8);
			gears.updateBase(Math.round((HXP.screen.width - ((2*scaledGearWidth) + (4*(scaledGearWidth* 0.94))))/2)+Math.round(scaledGearWidth/2), Math.round(HXP.screen.height * .03) + Math.round((scaledGearWidth/6)*5));
		}

		HelperFunctions.fullScreenFix();

		//graphic.scale = HXP.screen.height/2300;
		#end

		gridChecks();
		updateUI();

		super.update();
	}
}