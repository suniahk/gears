package scenes;

import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.graphics.Image;
import ui.UIElement;
import com.haxepunk.HXP;
import ui.MainButton;
import com.haxepunk.Tween;
import com.haxepunk.tweens.motion.LinearMotion;
import com.haxepunk.utils.Ease;
import entities.ClearEmitter;
import HelperFunctions;

class IntroScene extends Scene
{
	private var buttons : Array<Array<MainButton>>;
	private var camMove : LinearMotion;
	private var sceneTransition : Bool;
	private var cameraPage : Int;

	public function new()
	{
		super();
	}

	public function moveCamera (direction : Int)
	{
		if (!camMove.active)
		{
			if (direction == 1)
			{
				camMove.setMotion(HXP.camera.x, HXP.camera.y, HXP.camera.x+HXP.screen.width, HXP.camera.y, 1, Ease.bounceOut);
			} else if (direction == 0)
			{
				camMove.setMotion(HXP.camera.x, HXP.camera.y, HXP.camera.x-HXP.screen.width, HXP.camera.y, 1, Ease.bounceOut);
			}

			addTween(camMove);
		}
	}

	function checkMovement ()
	{
		if (camMove.active)
		{
			for (i in buttons)
			{
				for (j in i)
				{
					j.movement(true);
				}
			}
		} else {
			for (i in buttons)
			{
				for (j in i)
				{
					j.movement(false);
				}
			}
		}
	}


	public override function begin()
	{
		sceneTransition = false;

		buttons = [];
		buttons[0] = [];

		buttons[0][0] = new MainButton ("Start Game", Math.round(HXP.screen.width/2), Math.round(HXP.screen.height/3)*2, this);
		buttons[0][0].isMenuBase();

		buttons[1] = [];
		buttons[1][0] = new MainButton ("Zen Mode", Math.round(HXP.screen.width/2) + HXP.screen.width, Math.round(HXP.screen.height/3), this);
		buttons[1][1] = new MainButton ("Classic", Math.round(HXP.screen.width/2) + HXP.screen.width, buttons[1][0].y + Math.round(buttons[1][0].height * 1.2), this, "regular", true);
		buttons[1][2] = new MainButton ("Geardoku", Math.round(HXP.screen.width/2) + HXP.screen.width, buttons[1][1].y + Math.round(buttons[1][1].height * 1.2), this, "regular", true);
		buttons[1][3] = new MainButton ("Back", Math.round(HXP.screen.width/2) + HXP.screen.width, buttons[1][2].y + Math.round(buttons[1][2].height * 2), this);

		buttons[0][1] = new MainButton ("Options", Math.round(HXP.screen.width/2), buttons[0][0].y + Math.round(buttons[0][0].height * 1.2), this, "regular", true);
		buttons[0][1].isMenuBase();

		#if !mobile
		buttons[0][2] = new MainButton ("Exit Game", Math.round(HXP.screen.width/2), buttons[0][1].y + Math.round(buttons[0][1].height * 1.2), this);
		buttons[0][2].isMenuBase();
		#end
		addList(buttons[0]);

		addList(buttons[1]);

		camMove = new LinearMotion(TweenType.OneShot);
	}

	function clickResponse()
	{
		if (buttons[0][0].clicked())
        {
        	moveCamera(1);
        	cameraPage = 1;
        	buttons[0][0].changeFlag(true);
        } else if (buttons[1][3].clicked()) {
        	moveCamera(0);
        	cameraPage = 0;
        	buttons[0][0].changeFlag(false);
        } #if !mobile else if (buttons[0][2].clicked()) {
        	flash.system.System.exit(1);
        }#end else if (buttons[1][0].clicked()) {
        	sceneTransition = true;
        	moveCamera(1);
        }
	}

	private function updateButtonPositions()
	{
		if (buttons[0][0].flag())
		{
			buttons[0][0].changePosition(Math.round(HXP.screen.width/2), 50);

			buttons[0][1].changePosition(Math.round(HXP.screen.width/2), HXP.screen.height * 2);
			#if !mobile
				buttons[0][2].changePosition(Math.round(HXP.screen.width/2), HXP.screen.height * 2);
			#end
		} else {
			buttons[0][0].changePosition(Math.round(HXP.screen.width/2), Math.round(HXP.screen.height/3)*2);

			buttons[0][1].changePosition(Math.round(HXP.screen.width/2), buttons[0][0].y + Math.round(buttons[0][0].height * 1.2));
			#if !mobile
				buttons[0][2].changePosition(Math.round(HXP.screen.width/2), buttons[0][1].y + Math.round(buttons[0][1].height * 1.2));
			#end
		}

		#if !mobile
		buttons[1][0].changePosition(Math.round(HXP.screen.width/2) + HXP.screen.width, Math.round(HXP.screen.height/3));
		buttons[1][1].changePosition(Math.round(HXP.screen.width/2) + HXP.screen.width, buttons[1][0].y + Math.round(buttons[1][0].height * 1.2));
		buttons[1][2].changePosition(Math.round(HXP.screen.width/2) + HXP.screen.width, buttons[1][1].y + Math.round(buttons[1][1].height * 1.2));
		buttons[1][3].changePosition(Math.round(HXP.screen.width/2) + HXP.screen.width, buttons[1][2].y + Math.round(buttons[1][2].height * 2));

		if (cameraPage == 1)
		{
			camMove.x = HXP.screen.width;
		}
		#end
	}

	public override function update ()
	{
		// If any buttons are clicked, move the camera
		clickResponse();
		checkMovement();

		if (!camMove.active && sceneTransition) 
		{
			HXP.scene = new scenes.GameScene();
		}

		updateButtonPositions();
		HXP.camera.x = camMove.x;

		#if !mobile
        HelperFunctions.fullScreenFix();
		#end

		super.update();
	}
}